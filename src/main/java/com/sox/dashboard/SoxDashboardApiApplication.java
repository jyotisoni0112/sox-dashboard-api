package com.sox.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoxDashboardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoxDashboardApiApplication.class, args);
	}

}
